# Round Aggregates

The `RoundAggregates` object contains aggregate information about a group of rounds.

## Properties

### numRounds
The total number of rounds in the group.
* Type: `integer`
* Example: `45000`

### numHosts
The number of different players who hosted rounds in the group
* Type: `integer`
* Example: `368`

### numWinners
The number of different players who won rounds in the group
* Type: `integer`
* Example: `359`

### minRoundNumber
The minimum [roundNumber](/datatypes/round.md#roundNumber) for rounds in the group
* Type: `integer`
* Example: `35000`

### maxRoundNumber
The maximum [roundNumber](/datatypes/round.md#roundNumber) for rounds in the group
* Type: `integer`
* Example: `35000`

### allRoundNumbers
A sorted list of all [roundNumber](/datatypes/round.md#roundNumber)s for rounds in the group.
Note: This field cannot be used for filtering.
* Type: `integer[]`
* Example: `[35000, 40000, 45000]`

### minPostTime
The minimum [postTime](/datatypes/round.md#postTime) for rounds in the group, as a Unix time stamp
* Type: `integer`
* Example: `1507889585`

### maxPostTime
The maximum [postTime](/datatypes/round.md#postTime) for rounds in the group, as a Unix time stamp
* Type: `integer`
* Example: `1507889585`

### minWinTime
The minimum [winTime](/datatypes/round.md#winTime) for rounds in the group, as a Unix time stamp
* Type: `integer`
* Example: `1507889585`

### maxWinTime
The maximum [winTime](/datatypes/round.md#winTime) for rounds in the group, as a Unix time stamp
* Type: `integer`
* Example: `1507889585`

### allWinTimes
A sorted list of all [winTime](/datatypes/round.md#winTime)s for rounds in the group, as Unix time stamps.
Note: This field cannot be used for filtering.
* Type: `integer[]`
* Example: `[1507889585, 1507945207]`

### avgSolveTime
The average [solveTime](/datatypes/round.md#solveTime) for rounds in the group, in seconds
* Type `float`
* Example: `3567.4539`

### minSolveTime
The minimum [solveTime](/datatypes/round.md#solveTime) for rounds in the group, in seconds
* Type `integer`
* Example: `650`

### maxSolveTime
The maximum [solveTime](/datatypes/round.md#solveTime) for rounds in the group, in seconds
* Type `integer`
* Example: `650`

### avgPostDelay
The average [postDelay](/datatypes/round.md#postDelay) for rounds in the group, in seconds
* Type `float`
* Example: `3567.4539`

### minPostDelay
The minimum [postDelay](/datatypes/round.md#postDelay) for rounds in the group, in seconds
* Type `integer`
* Example: `650`

### maxPostDelay
The maximum [postDelay](/datatypes/round.md#postDelay) for rounds in the group, in seconds
* Type `integer`
* Example: `650`

### avgPlusCorrectDelay
The average [plusCorrectDelay](/datatypes/round.md#plusCorrectDelay) for rounds in the group, in seconds
* Type `float`
* Example: `3567.4539`

### minPlusCorrectDelay
The minimum [plusCorrectDelay](/datatypes/round.md#plusCorrectDelay) for rounds in the group, in seconds
* Type `integer`
* Example: `650`

### maxPlusCorrectDelay
The maximum [plusCorrectDelay](/datatypes/round.md#plusCorrectDelay) for rounds in the group, in seconds
* Type `integer`
* Example: `650`

## Additional Properties

If using [grouping](/endpoints/rounds-aggregate.md#groupBy), the corresponding property from the `Round` object will also be present in the `RoundAggregates` object.
