# Change Log

## 1.12.0

### Features

* Add support for grouping by time-based fields in the `/rounds/aggregate` endpoint, with settings for interval sizes
* Add `numHosts` and `numWinners` to the `RoundAggregates` type
* Add `/bulk` endpoint

### Deprecations

* The `/leaderboard/count` endpoint is now deprecated. The same functionality can now be achieved using `/rounds/aggregate`. This endpoint will continue to be supported until such time as I release a V2 API.

## 1.11.0

### Features

* Add `abandonedTime` property to the `Round` object

### Note

The behavior for the `abandoned` flag on the `Round` object remains the same as before, so there will be rounds with `abandoned = true` without an `abandonedTime` set.
The behavior may change in the future if I backfill the `abandonedTime` property.

## 1.10.2

### Features

* Add `?limit` query parameter to /rounds and /rounds/aggragate

## 1.10.0

### Features

* Add `/players` endpoint
* Add `includeWinTimes` query parameter to /leaderboard and `winTimeList` property on the response

## 1.9.0

### Features

* Add `allWinTimes` and `allRoundNumbers` properties to the `RoundAggregates` object

### Improvements

* Data for rounds before 31867 has now been backfilled

## 1.8.0

### Features

* Add `filterRounds` parameters to /leaderboard and /leaderboard/count
* Add `thumbnailUrl` property to the `Round` object

### Bug Fixes

* When sorting, null values will be returned at the end of the list instead of being ommitted.

### Deprecations

* Deprecate `fromRound`, `toRound`, `fromTime`, `toTime` parameters on /leaderboard and /leaderboard/count. These will still be supported but it's recommended to use `filterRounds` instead.

## 1.7.0

### Features

* Add /rounds/aggregate endpoint for fetching aggregate values for groups of rounds

## 1.6.0

### Features

* Add `postTimeOfDay`, `postDayOfWeek`, `winTimeOfDay`, `winDayOfWeek`, and `abandoned` properties to the `Round` object
    * These fields are also available to be used for filtering and sorting
* Add `cont` and `ncont` operators for filtering string fields by substring match
* Add `in` and `notin` operators for filtering fields by a set of acceptable values

### Bug Fixes

* Fix an issue with compound filter queries involving `postDelay` returning a 500 response

## 1.5.0

### Features

* Add `postDelay`, `solveTime`, and `plusCorrectDelay` properties to the `Round` object
    * These fields are also available to be used for filtering and sorting

## 1.4.0

### Features

* Add /rounds endpoint with filters and sorting

### Improvements

* Rewrite the docs using custom markdown
* Add extra information to some error responses

## 1.3.1

### Improvements

* Add support for query parameter array notation
* Update to Nodejs v10

## 1.3.0

### Features

* +correct time and title added to rounds
* /status endpoint: fetches version info about PictureGame services

## 1.2.1

## Improvements

* Send logs to Google Stackdriver
* Setup automated build through Gitlab CI

## 1.2.0

### Features

* Players and Ranks filters on the leaderboard
* /leaderboard/count endpoint: Fetches the number of players that have won rounds with the given filter
* Round filters on the leaderboard (round number range, date range)
* Ability to return only the win counts instead of the full lists of rounds from /leaderboard
* /view/current: Redirects to the current round on Reddit
* /view/round/N: Redirects to the given round on Reddit

### Improvements

* Replace internal SQL calls with Knex wrapper

## 1.1.0

### Features

* Authentication - command-line script to create user with username/password

### Improvements

* Migrate database to sqlite3
* Ensure API shuts down gracefully

## 1.0.2

### Bug Fixes

* Move leaderboard generation to a Python script to avoid issues with Snoowrap

## 1.0.0

### Features

* /rounds endpoint: Basic CRUD actions for individual rounds
* /current endpoint: fetches the current round
* /leaderboard endpoint: fetches the leaderboard, with optional parameters for rank range
* Rudimentary in-memory database backed by a CSV file
