# Rounds

The **Rounds** endpoint can be used to fetch information about a single round.

## Details

* Method: `GET`
* Path: `/rounds/{roundNumber}` where `roundNumber` is an integer

## Returns

A [Round](/datatypes/round.md) object containing all of the information that is currently on-hand for the round.

## Example Requests

* Get the information for round 40000:
    * `/rounds/40000`
