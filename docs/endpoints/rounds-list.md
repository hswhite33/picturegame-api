# List Rounds

The **Rounds List** endpoint can be used to fetch lists of rounds matching the specified criteria, with options to extract specific properties and sort rounds in specific orders.

**Note**: This endpoint returns a paginated list of rounds matching the search criteria. The page size limit may change at any time. If you are writing an integration with the API, please use the [pageSize](#pageSize) field on the response object to ensure you fetch all data correctly.

## Details

* Method: `GET`
* Path: `/rounds`

## Query Parameters

### filter
An expression to filter the returned rounds by. See [Filters](/guides/filters.md) for more information.
* Type: `string`
* Example: `?filter=winnerName eq "Provium"`

### select
A list of fields to include in the response. Values should be properties of the [Round](/datatypes/round.md) object. The `roundNumber` is always returned, and by default no other fields are returned. Can be specified in multiple different formats.
* Type: `string[]`
* Example: `?select=winnerName,id`
* Example: `?select[]=winnerName&select[]=id`

### sort
A list of fields to sort on, optionally specifying a direction (`asc` or `desc`, defaults to `asc`). Values should be properties of the [Round](/datatypes/round.md) object. Can be specified in multiple different formats.
* Type: `string[]`
* Example: `?sort=winnerName,hostName desc`
* Example: `?sort[]=winnerName&sort[]=hostName desc`

### limit
The maximum number of results to return in a single page. Page size may be limited to a value smaller than this; consult the [pageSize](#pageSize) field on the response object to determine how many results have actually been returned.
* Type: `integer`
* Example: `?limit=50`

### offset
The index of the first result to return (zero-indexed). Useful if the number of rounds matching your search is more than the page limit. Defaults to `0`
* Type: `integer`
* Example: `?offset=1000`

## Returns

A JSON object with the following properties:

### totalNumResults
The total number of rounds matching the given filter. If this value is greater than [pageSize](#pageSize), you will need to perform another request using [offset](#offset) to fetch additional results.
* Type: `integer`

### startIndex
The index of the first result (zero-indexed). Equivalent to the value of [offset](#offset).
* Type: `integer`

### pageSize
The maximum number of results to be included for a single request.
* Type: `integer`

### results
The rounds matching the filter, in the specified order. Only the fields specified in [select](#select) will be present on each round.
* Type: [Round](/datatypes/round.md)`[]`

## Example Requests

* Get a list of rounds won by Provium since the start of 2019:
    * `/rounds?filter=winnerName eq "Provium" and winTime gte 2019-01-01`
* Get a list of rounds hosted by TheLamestUsername and won by quatrevingtneuf, or vice versa:
    * `/rounds?filter=(hostName eq "TheLamestUsername" and winnerName eq "quatrevingtneuf") or (hostName eq "quatrevingtneuf" and winnerName eq "TheLamestUsername")`
