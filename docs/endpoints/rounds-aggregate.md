# List Round Aggregates

The **Rounds Aggregate** endpoint can be used to fetch aggregate values about groups of rounds matching specified criteria, with options to group by specific properties of the [Round](/datatypes/round.md), and filter the resulting groups.

**Note**: This endpoint returns a paginated list of rounds matching the search criteria. The page size limit may change at any time. If you are writing an integration with the API, please use the [pageSize](#pageSize) field on the response object to ensure you fetch all data correctly.

## Details

* Method: `GET`
* Path: `/rounds/aggregate`

## Query Parameters

### filter
An expression to filter the rounds to be considered in the aggregates. See [Filters](/guides/filters.md) for more information.
* Type: `string`
* Example: `?filter=postTime gte 2019-01-01`

### select
A list of fields to include in the response. Values should be properties of the [RoundAggregates](/datatypes/round-aggregates.md) object. The `numRounds` field is always returns, and by default no other fields are returned. Can be specified in multiple different formats/
* Type: `string[]`
* Example: `?select=minSolveTime,maxPlusCorrectDelay`
* Example: `?select[]=minSolveTime&select[]=maxPlusCorrectDelay`

### groupBy
A field on the [Round](/datatypes/round.md) and, in some cases, a list of arguments to specify how the resulting aggregates should be grouped.
If specified, the result will contain a [RoundAggregates](/datatypes/round-aggregates.md) object for each value of the specified field.

See [Grouping](/guides/grouping.md) for a detailed guide on using this parameter.

The following fields can be used for grouping aggregates:

| Field | Data Type | Description |
|:--|:--|:--|
| [hostName](/datatypes/round.md#hostName) | [Exact Match](/guides/grouping.md#exact-match) | Group rounds by their host. |
| [winnerName](/datatypes/round.md#winnerName) | [Exact Match](/guides/grouping.md#exact-match) | Group rounds by their winner. |
| [postDayOfWeek](/datatypes/round.md#postDayOfWeek) | [Exact Match](/guides/grouping.md#exact-match) | Group rounds by the day of the week on which they were posted. |
| [winDayOfWeek](/datatypes/round.md#winDayOfWeek) | [Exact Match](/guides/grouping.md#exact-match) | Group rounds by the day of the week on which they were won. |
| [postTime](/datatypes/round.md#postTime) | [Absolute Time](/guides/grouping.md#absolute-time) | Group rounds by the absolute time they were posted. |
| [winTime](/datatypes/round.md#winTime) | [Absolute Time](/guides/grouping.md#absolute-time) | Group rounds by the absolute time they were won. |
| [postTimeOfDay](/datatypes/round.md#postTimeOfDay) | [Relative Time](/guides/grouping.md#relative-time) | Group rounds by the time of day they were posted. |
| [winTimeOfDay](/datatypes/round.md#winTimeOfDay) | [Relative Time](/guides/grouping.md#relative-time) | Group rounds by the time of day they were won. |
| [postDelay](/datatypes/round.md#postDelay) | [Duration](/guides/grouping.md#duration) | Group rounds by time taken to post. |
| [solveTime](/datatypes/round.md#solveTime) | [Duration](/guides/grouping.md#duration) | Group rounds by time taken to solve. |
| [plusCorrectDelay](/datatypes/round.md#plusCorrectDelay) | [Duration](/guides/grouping.md#duration) | Group rounds by time taken to award the +correct. |

* Type: `string`
* Example: `?groupBy=winnerName`
* Example: `?groupBy=solveTime(30, minute)`

### groupSort
A list of fields to sort the resulting groups on, optionally specifying a direction (`asc` or `desc`; defaults to `asc`). Values should be properties of the [RoundAggregates](/datatypes/round-aggregates.md) object. Can be specified in multiple different formats. Ignored if [groupBy](#groupBy) is not specified.
* Type: `string[]`
* Example: `?groupSort=numRounds desc,maxRoundNumber`
* Example: `?groupSort[]=numRounds desc&sort[]=maxRoundNumber`

### groupFilter
An expression to filter the resulting groups by. Ignored if [groupBy](#groupBy) is not specified. See [Filters](/guides/filters.md) for more information.
* Type `string`
* Example: `?groupFilter=avgPostDelay lte 300`

### limit
The maximum number of results to return in a single page. Page size may be limited to a value smaller than this; consult the [pageSize](#pageSize) field on the response object to determine how many results have actually been returned.
* Type: `integer`
* Example: `?limit=50`

### offset
The index of the first result to return (zero-indexed). Useful if the number of rounds matching your search is more than the page limit. Defaults to `0`
* Type: `integer`
* Example: `?offset=1000`

## Returns

A JSON object with the following properties:

### totalNumResults
The total number of groups of aggregates. If [groupBy](#groupBy) is not specified, this value will always be `1`. If this value is greater than [pageSize](#pageSize), you will need to perform another request using [offset](#offset) to fetch additional results.
* Type: `integer`

### startIndex
The index of the first result (zero-indexed). Equivalent to the value of [offset](#offset).
* Type: `integer`

### pageSize
The maximum number of results to be included for a single request.
* Type: `integer`

### results
The aggregate values for the resulting groups of rounds. Only the fields specified in [select](#select) will be present on each round.
* Type: [RoundAggregates](/datatypes/round-aggregates.md)`[]`
