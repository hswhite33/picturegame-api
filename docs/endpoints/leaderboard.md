# Leaderboard

The **Leaderboard** endpoint can be used to generate leaderboards based on a subset of rounds, or a subset or players, or both.

By default, the leadboard will be generated from all rounds, and return the stats for all players.

If you only need to know how many players have won rounds for a certain range of rounds, use [Player Counts](/endpoints/player-count.md)

## Details

* Method: `GET`
* Path: `/leaderboard`

## Query Parameters

### filterRounds
An expression by which to filter the rounds included in the leaderboard. See [Filters](/guides/filters.md) for more information.
* Type: `string`
* Example: `?filter=postTime gte 2019-01-01`

### *fromRound*
The round number for the earliest round to include in the leaderboard (inclusive)

**Deprecated**: Use `filterRounds` instead
* Type: `integer`
* Example: `?fromRound=50000`

### *toRound*
The round number for the latest round to include in the leaderboard (inclusive)

**Deprecated**: Use `filterRounds` instead
* Type: `integer`
* Example: `?toRound=50000`

### *fromTime*
Only include rounds that were won at or after this time. Specified as an ISO-8601 DateTime string, defaulting to UTC.

**Deprecated**: Use `filterRounds` instead
* Type: `string`
* Example: `?fromTime=2019-01-01`

### *toTime*
Only include rounds that were won at or before this time. Specified as an ISO-8601 DateTime string, defaulting to UTC.

**Deprecated**: Use `filterRounds` instead
* Type: `string`
* Example: `?toTime=2019-01-31T23:59:59`

### fromRank
The first rank to fetch. Defaults to rank 1.
* Type: `integer`
* Example: `?fromRank=26`

### count
The number of players to fetch, starting from `fromRank`. Defaults to everyone.
* Type: `integer`
* Example: `?count=25`

### players
A list of players to fetch. Can be specified in multiple different formats (see below).
* Type: `string[]`
* Example: `?players=provium,superfreakonomics,quatrevingtneuf`
* Example: `?players[]=provium&players[]=superfreakonomics&players[]=quatrevingtneuf`

### ranks
A list of ranks to fetch. Can be specified in multiple different formats (see below).
* Type: `string[]`
* Example: `?ranks=100,200,300`
* Example: `?ranks[]=100&ranks[]=200&ranks[]=300`

### includeRoundNumbers
Whether to include a list of solved round numbers on each returned [LeaderboardEntry](/datatypes/leaderbaord-entry.md) object. Defaults to `true`.
It's strongly recommended to set this parameter to `false` unless you really need round numbers.
* Type: `boolean`
* Example: `?includeRoundNumbers=false`

### includeWinTimes
Whether to include a list of times for each solved round on each returned LeaderboardEntry object. Defaults to `false`.
* Type: `boolean`
* Example: `?includeWinTimes=true`

## Returns

A JSON object with the following properties:

### leaderboard
The list of players matching your search criteria, sorted by rank.
* Type: [LeaderboardEntry](/datatypes/leaderboard-entry.md)`[]`

### invalidRanks
A list of invalid ranks specified in the [ranks](#ranks) parameter, if any. Ranks are invalid either if they cannot be parsed as an integer, or if no player exists at the given rank.

### invalidUsernames
A list of invalid usernames specified in the [players](#players) parameter, if any. Players are invalid if nobody with the given username has won any rounds in the searched round range.

## Example Requests

* Get the full leaderboard, without round numbers:
    * `/leaderboard?includeRoundNumbers=false`
* Get the leaderboard for rounds in 2018:
    * `/leaderboard?filterRounds=winTime gte 2018-01-01 and winTime lt 2019-01-01`
* Get the leaderboard for rounds between 40000 and 49999 (inclusive):
    * `/leaderboard?filterRounds=roundNumber gte 40000 and roundNumber lt 50000`
* Get the top 25 leaderboard for all time:
    * `/leaderboard?count=25`
