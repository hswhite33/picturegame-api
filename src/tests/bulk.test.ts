import * as request from 'supertest';

import { Application } from '../application';
import { BulkRequest, SingleLeaderboardRequest, SingleRoundsAggregateRequest, SingleRoundsRequest } from '../models/model';

import * as utils from './testutils';

describe('Bulk request tests', () => {
    let app: Application;

    beforeAll(async () => {
        app = await utils.createAppWithData();
    });

    afterAll(async () => {
        await app.teardown();
    });

    test('leaderboard, rounds, round aggs', async () => {
        const lbReq: SingleLeaderboardRequest = {
            endpoint: 'leaderboard',
            includeRoundNumbers: false,
            count: 5,
        };
        const roundsReq: SingleRoundsRequest = {
            endpoint: 'rounds',
            filter: 'hostName eq "chuttiekang"',
            select: ['postTime'],
        };
        const aggsReq: SingleRoundsAggregateRequest = {
            endpoint: 'rounds/aggregate',
            groupBy: 'hostName',
            select: ['minSolveTime'],
        };
        const req: BulkRequest = {
            requests: [lbReq, roundsReq, aggsReq],
        };
        const res = await request(app.app).post('/bulk').send(req).expect(200);
        expect(res.body).toMatchSnapshot();
    });

    test('validation errors in one, other completes successfully', async () => {
        const lbReq: SingleLeaderboardRequest = {
            endpoint: 'leaderboard',
            includeRoundNumbers: false,
            count: -5,
        };
        const roundsReq: SingleRoundsRequest = {
            endpoint: 'rounds',
            filter: 'hostName eq "chuttiekang"',
            select: ['postTime'],
        };
        const req: BulkRequest = {
            requests: [lbReq, roundsReq],
        };
        const res = await request(app.app).post('/bulk').send(req).expect(200);
        expect(res.body).toMatchSnapshot();
    });

    test('invalid filter in one, other completes successfully', async () => {
        const lbReq: SingleLeaderboardRequest = {
            endpoint: 'leaderboard',
            includeRoundNumbers: false,
            filterRounds: 'idk what to put here',
        };
        const roundsReq: SingleRoundsRequest = {
            endpoint: 'rounds',
            filter: 'hostName eq "chuttiekang"',
            select: ['postTime'],
        };
        const req: BulkRequest = {
            requests: [lbReq, roundsReq],
        };
        const res = await request(app.app).post('/bulk').send(req).expect(200);
        expect(res.body).toMatchSnapshot();
    });

    test('invalid endpoint, other completes successfully', async () => {
        const roundsReq: SingleRoundsRequest = {
            endpoint: 'rounds',
            filter: 'hostName eq "chuttiekang"',
            select: ['postTime'],
        };
        const badReq = {
            endpoint: 'invalid',
        };
        const req = {
            requests: [roundsReq, badReq],
        };
        const res = await request(app.app).post('/bulk').send(req).expect(200);
        expect(res.body).toMatchSnapshot();
    });

    test('missing requests array', async () => {
        const res = await request(app.app).post('/bulk').send({ foo: 'bar' }).expect(400);
        expect(res.body).toMatchSnapshot();
    });

    test('empty requests array', async () => {
        const res = await request(app.app).post('/bulk').send({ requests: [] }).expect(400);
        expect(res.body).toMatchSnapshot();
    });

    test('single request is not an object', async () => {
        const res = await request(app.app).post('/bulk').send({ requests: [1] }).expect(200);
        expect(res.body).toMatchSnapshot();
    });
});
