import * as request from 'supertest';

import { Application } from '../application';
import { BadRequestError } from '../models/Error';
import { ListResponse, Round } from '../models/model';

import * as utils from './testutils';

type RoundListResponse = ListResponse<Round>;

describe('GET rounds with queries', () => {
    let app: Application;

    function configure(opts?: utils.ConfigureTestOpts) {
        beforeAll(async () => {
            app = await utils.createAppWithData(opts);
        });

        afterAll(async () => {
            await app.teardown();
        });
    }

    describe('?filter', () => {
        configure();

        it('should filter the rounds when using ?filter', async () => {
            const res = await request(app.app).get('/rounds?filter=roundNumber gte 50197').expect(200);
            const body = res.body as RoundListResponse;
            expect(body.results.map(r => r.roundNumber).sort()).toEqual([50197, 50198, 50199]);
        });

        it('should be able to filter by submission id', async () => {
            const res = await request(app.app).get('/rounds?filter=id eq "8y4n8y"').expect(200);
            const body = res.body as RoundListResponse;
            expect(body.results.length).toBe(1);
            expect(body.results[0].roundNumber).toBe(50000);
        });

        it('should return 400 for query parsing errors', async () => {
            const res = await request(app.app).get('/rounds?filter=id').expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.QueryParseError, {
                innerError: 'Unexpected end of input',
            }));
        });

        it('should support different url encodings', async () => {
            let res = await request(app.app).get('/rounds?filter=roundNumber+gte+50197').expect(200);
            let body = res.body as RoundListResponse;
            expect(body.results.map(r => r.roundNumber).sort()).toEqual([50197, 50198, 50199]);

            res = await request(app.app).get('/rounds?filter=roundNumber%20gte%2050197').expect(200);
            body = res.body as RoundListResponse;
            expect(body.results.map(r => r.roundNumber).sort()).toEqual([50197, 50198, 50199]);
        });

        it('should be able to filter by calculated values', async () => {
            const res = await request(app.app).get('/rounds?filter=postDelay lte 60').expect(200);
            const body = res.body as RoundListResponse;
            // only 5 rounds in this range with <= 1minute delay :(
            expect(body.results.map(r => r.roundNumber).sort()).toEqual([50015, 50160, 50180, 50190, 50193]);
        });

        it('should work with compound queries involving joins', async () => {
            const res = await request(app.app)
                .get('/rounds?filter=postDelay lte 60 and roundNumber lt 50100')
                .expect(200);

            const body = res.body as RoundListResponse;
            expect(body.results.map(r => r.roundNumber)).toEqual([50015]);
        });

        it('should support contains for substring matching', async () => {
            const res = await request(app.app).get('/rounds?filter=title cont "binomial"&select=title').expect(200);
            const body = res.body as RoundListResponse;

            expect(body.totalNumResults).toBe(12);
            for (const round of body.results) {
                expect(round.title!.toLowerCase().includes('binomial')).toBe(true);
            }
        });

        it('should support "in" array membership filter', async () => {
            const res = await request(app.app).get('/rounds?filter=roundNumber in [50001, 50004, 50010]').expect(200);
            const body = res.body as RoundListResponse;

            expect(body.totalNumResults).toBe(3);
            expect(body.results.map(r => r.roundNumber)).toEqual([50001, 50004, 50010]);
        });

        it('should support "notin" array complement filter', async () => {
            const notinRes = await request(app.app)
                .get('/rounds?filter=winDayOfWeek notin [1,2,3,4,5,6]&sort=roundNumber')
                .expect(200);
            const notinBody = notinRes.body as RoundListResponse;

            const eqRes = await request(app.app).get('/rounds?filter=winDayOfWeek eq 0&sort=roundNumber').expect(200);
            const eqBody = eqRes.body as RoundListResponse;

            expect(notinBody.totalNumResults).toBe(eqBody.totalNumResults);
            expect(notinBody.results.map(r => r.roundNumber)).toEqual(eqBody.results.map(r => r.roundNumber));
        });
    });

    describe('?select', () => {
        configure();

        it('should only return round numbers by default', async () => {
            const res = await request(app.app).get('/rounds?filter=roundNumber eq 50000').expect(200);
            const body = res.body as RoundListResponse;
            expect(body.results[0]).toEqual({ roundNumber: 50000 });
        });

        it('should return the selected fields + roundNumber', async () => {
            const res = await request(app.app).get('/rounds?filter=roundNumber eq 50000&select=winnerName').expect(200);
            const body = res.body as RoundListResponse;
            expect(body.results[0]).toEqual({ roundNumber: 50000, winnerName: 'Tephrite' });
        });

        it('should return 400 for poorly structured select statement', async () => {
            const res = await request(app.app).get('/rounds?select=winnerName eq').expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.QueryParseError, {
                innerError: 'Elements in parameter ?select must be single identifiers.',
            }));
        });

        it('should return 400 for unknown identifier in select statement', async () => {
            const res = await request(app.app).get('/rounds?select=NonExistentField').expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.QueryParseError, {
                innerError: 'Unknown identifier NonExistentField at index 0',
            }));
        });

        it('should be able to select calculated values', async () => {
            const filter = 'filter=roundNumber eq 50001';
            const select = 'select=postDelay,plusCorrectDelay,solveTime';
            const res = await request(app.app).get(`/rounds?${filter}&${select}`).expect(200);
            const body = res.body as RoundListResponse;
            expect(body.results[0]).toEqual({
                roundNumber: 50001,
                postDelay: 95,
                solveTime: 1417,
                plusCorrectDelay: 44,
            });
        });
    });

    describe('?sort', () => {
        configure();

        it('should sort on the specified column, default asc', async () => {
            const res = await request(app.app)
                // 50000 - 50004: five different hosts, no collisions
                .get('/rounds?filter=roundNumber lt 50005&sort=hostName&select=hostName')
                .expect(200);
            expect(res.body).toMatchSnapshot();
        });

        it('should sort desc if specified', async () => {
            const res = await request(app.app)
                // 50000 - 50004: five different hosts, no collisions
                .get('/rounds?filter=roundNumber lt 50005&sort=hostName desc&select=hostName')
                .expect(200);
            expect(res.body).toMatchSnapshot();
        });

        it('should fall back to the second sort dir', async () => {
            // 50009 - 50016: 3 rounds hosted by SuperFreakonomics
            // Each has a different winner to break the collision
            const filter = 'roundNumber gt 50008 and roundNumber lt 50017';
            const res = await request(app.app)
                .get(`/rounds?filter=${filter}&sort=hostName,winnerName&select=hostName,winnerName`)
                .expect(200);
            expect(res.body).toMatchSnapshot();
        });

        it('should return 400 for poorly structured sort', async () => {
            const res = await request(app.app).get('/rounds?sort=roundNumber id').expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.QueryParseError, {
                innerError: 'Invalid sort: expected IDENTIFIER [DIRECTION]',
            }));
        });

        it('should return 400 for invalid identifier', async () => {
            const res = await request(app.app).get('/rounds?sort=NonExistentField').expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.QueryParseError, {
                innerError: 'Unknown identifier NonExistentField at index 0',
            }));
        });

        it('should return 400 for poorly structured sort', async () => {
            const res = await request(app.app).get('/rounds?sort=roundNumber asc foo').expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.QueryParseError, {
                innerError: 'Invalid sort: expected IDENTIFIER [DIRECTION]',
            }));
        });

        it('should return 400 for incorrect sort value type', async () => {
            const res = await request(app.app).get('/rounds?sort=1').expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.QueryParseError, {
                innerError: 'Invalid sort: expected IDENTIFIER [DIRECTION]',
            }));
        });

        it('should be able to sort on calculated values', async () => {
            const res = await request(app.app).get('/rounds?sort=postDelay asc').expect(200);
            const body = res.body as RoundListResponse;

            // Edge case: by default SQLite would return 50000 at the top of the list as postDelay is null
            // Instead, we filter it out
            expect(body.results.map(r => r.roundNumber))
                .toEqual([50193, 50015, 50160, 50180, 50190, 50170, 50097, 50061, 50178, 50159]);
        });
    });

    describe('sorting with nulls', () => {
        configure({ dataFile: 'MockData_NullValues.csv' });

        it('should sort nulls at the end for asc', async () => {
            const res = await request(app.app).get('/rounds?sort=solveTime asc').expect(200);
            const body = res.body as RoundListResponse;

            // 5 rounds; 2 of them have solveTime = null
            // Those with null have indeterminate sort order

            const roundNumbers = body.results.map(r => r.roundNumber);
            expect(roundNumbers.slice(0, 3)).toEqual([5, 4, 1]);
            expect(roundNumbers.slice(3)).toEqual(expect.arrayContaining([2, 3]));
        });

        it('should sort nulls at the end for desc', async () => {
            const res = await request(app.app).get('/rounds?sort=solveTime desc').expect(200);
            const body = res.body as RoundListResponse;

            const roundNumbers = body.results.map(r => r.roundNumber);
            expect(roundNumbers.slice(0, 3)).toEqual([1, 4, 5]);
            expect(roundNumbers.slice(3)).toEqual(expect.arrayContaining([2, 3]));
        });
    });

    describe('pagination', () => {
        const PageSize = 2;
        configure({ pageSizeLimit: PageSize });

        it('should return up to the configured number of elements', async () => {
            const res = await request(app.app).get('/rounds?sort=roundNumber').expect(200);
            const body = res.body as RoundListResponse;
            expect(body.totalNumResults).toBe(200);
            expect(body.startIndex).toBe(0);
            expect(body.pageSize).toBe(PageSize);
            expect(body.results.length).toBe(PageSize);
            expect(body.results.map(r => r.roundNumber)).toEqual([50000, 50001]);
        });

        it('should return the next $pageSize elements starting from offset', async () => {
            const res = await request(app.app).get('/rounds?sort=roundNumber&offset=2').expect(200);
            const body = res.body as RoundListResponse;
            expect(body.totalNumResults).toBe(200);
            expect(body.startIndex).toBe(2);
            expect(body.pageSize).toBe(PageSize);
            expect(body.results.length).toBe(PageSize);
            expect(body.results.map(r => r.roundNumber)).toEqual([50002, 50003]);
        });

        it('should respect the limit specified by query param', async () => {
            const res = await request(app.app).get('/rounds?sort=roundNumber&limit=1').expect(200);
            const body = res.body as RoundListResponse;
            expect(body.totalNumResults).toBe(200);
            expect(body.startIndex).toBe(0);
            expect(body.pageSize).toBe(1);
            expect(body.results.length).toBe(1);
            expect(body.results.map(r => r.roundNumber)).toEqual([50000]);
        });

        it('should truncate the limit specified by query param if greater than config', async () => {
            const res = await request(app.app).get('/rounds?sort=roundNumber&limit=100').expect(200);
            const body = res.body as RoundListResponse;
            expect(body.totalNumResults).toBe(200);
            expect(body.startIndex).toBe(0);
            expect(body.pageSize).toBe(PageSize);
            expect(body.results.length).toBe(PageSize);
            expect(body.results.map(r => r.roundNumber)).toEqual([50000, 50001]);
        });

        it('should return nothing if offset is greater than the number of results', async () => {
            const res = await request(app.app).get('/rounds?sort=roundNumber&offset=200').expect(200);
            const body = res.body as RoundListResponse;
            expect(body.totalNumResults).toBe(200);
            expect(body.startIndex).toBe(200);
            expect(body.pageSize).toBe(PageSize);
            expect(body.results.length).toBe(0);
        });

        it('should not fail if offset is 0', async () => {
            await request(app.app).get('/rounds?offset=0').expect(200);
        });
    });

    describe('calculated values needing a table join', () => {
        configure({ dataFile: 'MockData_PrevRoundJoins.csv' });

        test('no previous round exists', async () => {
            const res = await request(app.app).get('/rounds?select=postDelay&filter=roundNumber eq 1');
            const body = res.body as RoundListResponse;
            expect(body.totalNumResults).toBe(1);

            // round is included in the result, but postDelay is not present
            expect(body.results[0]).toEqual({ roundNumber: 1 });
        });

        test('round is not hosted by the previous round\'s winner', async () => {
            const res = await request(app.app).get('/rounds?select=postDelay&filter=roundNumber eq 2');
            const body = res.body as RoundListResponse;
            expect(body.totalNumResults).toBe(1);
            expect(body.results[0]).toEqual({ roundNumber: 2 });
        });

        test('round starts before previous round was +corrected', async () => {
            const res = await request(app.app).get('/rounds?select=postDelay&filter=roundNumber eq 3');
            const body = res.body as RoundListResponse;
            expect(body.totalNumResults).toBe(1);
            expect(body.results[0]).toEqual({ roundNumber: 3 });
        });

        test('round joins correctly', async () => {
            const res = await request(app.app).get('/rounds?select=postDelay&filter=roundNumber eq 4');
            const body = res.body as RoundListResponse;
            expect(body.totalNumResults).toBe(1);
            expect(body.results[0]).toEqual({ roundNumber: 4, postDelay: 36 });
        });
    });

    describe('abandoned flag', () => {
        configure({ dataFile: 'MockData_AbandonedFlag.csv' });

        async function getRound(roundNumber: number): Promise<Round> {
            const res = await request(app.app)
                .get(`/rounds?filter=roundNumber eq ${roundNumber}&select=abandoned`)
                .expect(200);
            return (res.body as RoundListResponse).results[0];
        }

        test('abandoned if plusCorrectTime after following round postTime', async () => {
            const round = await getRound(2);
            expect(round.abandoned).toBe(true);
        });

        test('abandoned if plusCorrectTime is null', async () => {
            const round = await getRound(4);
            expect(round.abandoned).toBe(true);
        });

        test('not abandoned if it is the current round', async () => {
            const round = await getRound(6);
            expect(round.abandoned).toBe(false);
        });

        test('not abandoned if plusCorrectTime before postTime of next round', async () => {
            const round = await getRound(1);
            expect(round.abandoned).toBe(false);
        });
    });

    describe('abandoned flag edge case', () => {
        configure();

        test('not abandoned if the current round is over', async () => {
            const res = await request(app.app)
                .get('/rounds?filter=roundNumber eq 50199&select=abandoned')
                .expect(200);
            const body = res.body as RoundListResponse;
            expect(body.results[0].abandoned).toBe(false);
        });
    });
});
