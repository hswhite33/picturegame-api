import * as _ from 'lodash';
import * as path from 'path';

import { configureLogging, Application } from '../../application';
import { DbWrapper } from '../../db/DbWrapper';
import { Config, LogConfig, RecursivePartial } from '../../index.d';
import { importCsv } from '../../scripts/ImportCsv';

const DefaultTestConfig: RecursivePartial<Config> = {
    api: {
        authBypass: true,
        logging: {
            console: false,
        },
    },
    test: {
    },
};

export async function getTestServer(config?: RecursivePartial<Config>, ...sql: string[]): Promise<Application> {
    config = _.defaultsDeep({}, config, DefaultTestConfig) as RecursivePartial<Config>;
    const db = await getTestDb(config, ...sql);
    const app = await Application.setup(config, db);
    app.start();
    return app;
}

export async function getTestDb(config?: RecursivePartial<Config>, ...sql: string[]) {
    config = _.defaultsDeep({}, config, DefaultTestConfig) as RecursivePartial<Config>;
    configureLogging(config.api!.logging as LogConfig);
    const wrapper = await DbWrapper.setup('99.99.99-dev.unittests', ':memory:');
    await wrapper.execSql(sql);
    return wrapper;
}

export interface ConfigureTestOpts {
    pageSizeLimit?: number;
    dataFile?: string;
}
export async function createAppWithData(opts: ConfigureTestOpts = {}) {
    const app = await getTestServer({
        api: {
            authBypass: true,
            pageSizeLimit: opts.pageSizeLimit ?? 10,
        },
    });
    const filePath = path.join(__dirname, '..', 'testdata', opts.dataFile ?? 'LiveData50000-50199.csv');
    await importCsv(app, filePath, false);
    return app;
}
