import * as request from 'supertest';

import { Application } from '../application';
import { BadRequestError, NotFoundError } from '../models/Error';

import * as utils from './testutils';

describe('/players integration tests', () => {
    let app: Application;

    describe('/migrate', () => {
        beforeAll(async () => {
            app = await utils.getTestServer({},
                'INSERT INTO rounds(round_number, winner_name) VALUES (1, "a");',
                'INSERT INTO rounds(round_number, winner_name) VALUES (2, "b");',
                'INSERT INTO rounds(round_number, winner_name, host_name) VALUES (3, "b", "c");',
                'INSERT INTO rounds(round_number, winner_name) VALUES (4, "G");',
            );
        });

        afterAll(async () => {
            await app?.teardown();
        });

        it('should return 400 if usernames are missing', async () => {
            let res = await request(app.app).post('/players/migrate').expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.ValueRequired, {
                fieldName: 'fromName',
            }));

            res = await request(app.app).post('/players/migrate?fromName=a').expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.ValueRequired, {
                fieldName: 'toName',
            }));
        });

        it('should return 400 if  usernames are invalid', async () => {
            let res = await request(app.app).post('/players/migrate?fromName=!@@#$').expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.InvalidUsername, {
                fieldName: 'fromName',
            }));

            res = await request(app.app).post('/players/migrate?fromName=a&toName=!@@#$').expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.InvalidUsername, {
                fieldName: 'toName',
            }));
        });

        it('should return the target player\'s wins', async () => {
            const res = await request(app.app).post('/players/migrate?fromName=b&toName=d').expect(200);
            expect(res.body.newPlayer).toBeDefined();
            expect(res.body.newPlayer.roundList).toEqual([2, 3]);
            expect(res.body.newPlayer.numWins).toBe(2);
        });

        it('should change fields on rounds', async () => {
            await request(app.app).post('/players/migrate?fromName=a&toName=e').expect(200);
            let res = await request(app.app).get('/rounds/1').expect(200);
            expect(res.body.winnerName).toBe('e');

            await request(app.app).post('/players/migrate?fromName=c&toName=f').expect(200);
            res = await request(app.app).get('/rounds/3').expect(200);
            expect(res.body.hostName).toBe('f');
        });

        it('should succeed with incorrect case', async () => {
            const res = await request(app.app).post('/players/migrate?fromName=g&toName=h').expect(200);
            expect(res.body.newPlayer).toBeDefined();
            expect(res.body.newPlayer.roundList).toEqual([4]);
        });

        it('should fail if the fromName doesn\'t exist', async () => {
            const res = await request(app.app)
                .post('/players/migrate?fromName=NotARealPlayer&toName=ANewPlayer').expect(404);
            expect(res.body).toEqual(new NotFoundError(NotFoundError.PlayerNotFound));
        });
    });

    describe('fetching player data', () => {
        beforeAll(async () => {
            app = await utils.createAppWithData();
        });

        afterAll(async () => {
            await app.teardown();
        });

        it('should return 400 if names are not specified', async () => {
            const res = await request(app.app).get('/players').expect(400);
            expect(res.body.errorCode).toBe('VALUE_REQUIRED');
            expect(res.body.data.fieldName).toBe('names');
        });

        it('should skip names that do not exist', async () => {
            const res = await request(app.app).get('/players?names=blahblahblahblah').expect(200);
            expect(res.body.totalNumResults).toBe(0);
            expect(res.body.results).toEqual([]);
        });

        it('should return all stats for the given players', async () => {
            const res = await request(app.app).get('/players?names=chuttiekang,bryberg').expect(200);
            expect(res.body).toMatchSnapshot();
        });

        it('should filter rounds included in the metrics', async () => {
            const filter = 'roundNumber gte 50010 and roundNumber lt 50020';
            const res = await request(app.app).get(`/players?names=phenomist&filterRounds=${filter}`).expect(200);
            expect(res.body).toMatchSnapshot();
        });

        it('should not include players who have hosted but not won any rounds', async () => {
            const filter = 'roundNumber gte 50010 and roundNumber lt 50020';
            const res = await request(app.app).get(`/players?names=i_luke_tirtles&filterRounds=${filter}`).expect(200);
            expect(res.body.totalNumResults).toBe(0);
            expect(res.body.results).toEqual([]);
        });
    });
});
