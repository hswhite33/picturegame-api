export const Thursday = 4; // 1970-01-01 was on a Thursday, so offset day of week by 4

export const secPerMinute = 60;
export const secPerHour = secPerMinute * 60;
export const secPerDay = secPerHour * 24;

export const dayPerWeek = 7;
export const monthPerYear = 12;

export function formatTimeDiff(diffSeconds: number) {
    const secs = diffSeconds % 60;
    diffSeconds = Math.floor(diffSeconds / 60);

    const mins = diffSeconds % 60;
    diffSeconds = Math.floor(diffSeconds / 60);

    const hours = diffSeconds % 24;
    const days = Math.floor(diffSeconds / 24);

    let result = 'P';
    if (days) {
        result += `${days}D`;
    }
    if (hours || mins || secs) {
        result += 'T';
    }
    if (hours) {
        result += `${hours}H`;
    }
    if (mins) {
        result += `${mins}M`;
    }
    if (secs) {
        result += `${secs}S`;
    }
    return result;
}

export function YearToUnixTimestamp(year: number) {
    return Math.floor(new Date(Date.UTC(year, 0, 1)).valueOf() / 1000);
}

export function MonthToUnixTimestamp(month: number) {
    const date = new Date(Date.UTC(1970, 0, 1));
    date.setUTCMonth(month);
    return Math.floor(date.valueOf() / 1000);
}

export function WeekToUnixTimestamp(week: number) {
    return (week * dayPerWeek - Thursday) * secPerDay;
}
