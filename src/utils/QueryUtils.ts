import { QueryParseError } from 'picturegame-api-wrapper';
import { BadRequestError } from '../models/Error';

export function runWithBadRequest<T>(action: () => T): T {
    try {
        return action();
    } catch (e) {
        if (e instanceof QueryParseError) {
            throw new BadRequestError(BadRequestError.QueryParseError, {
                innerError: e.message,
            });
        }
        throw e;
    }
}
