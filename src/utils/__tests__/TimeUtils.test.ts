import { formatTimeDiff } from '../TimeUtils';

describe('TimeUtils', () => {
    describe('formatTimeDiff', () => {
        const tests: [number, string][] = [
            [45, 'PT45S'],
            [120, 'PT2M'],
            [7200, 'PT2H'],
            [86400, 'P1D'],
            [90000, 'P1DT1H'],
        ];
        for (const testData of tests) {
            test(`${testData[0]} should format to ${testData[1]}`, () => {
                expect(formatTimeDiff(testData[0])).toBe(testData[1]);
            });
        }
    });
});
