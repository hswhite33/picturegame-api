import * as express from 'express';

import { Config } from '../index.d';

import { DbWrapper } from '../db';

import { authenticate } from './AuthMiddleware';
import Endpoint from './Endpoint';

export function configureStatus(config: Config, db: DbWrapper): express.Router {
    const endpoint = new StatusPage(config, db);
    const router = express.Router();
    const auth = authenticate(config.api.authBypass);

    router.get('/', endpoint.getStatus);
    router.post('/', auth, endpoint.postStatus);

    return router;
}

class StatusPage extends Endpoint {
    getStatus = this.configureEndpoint(async (_, res) => {
        const statuses = await this.db.status.getAllStatus();
        res.json({
            'services': statuses,
        });
    });

    postStatus = this.configureEndpoint(async (req, res) => {
        const status = await this.db.status.setStatus(req.body);
        res.json(status);
    });
}
