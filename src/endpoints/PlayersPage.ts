import * as express from 'express';

import { Config, Response } from '../index.d';

import { RoundsController } from '../controllers';
import { DbWrapper } from '../db';

import { LeaderboardEntry } from '../models/model';
import * as Query from '../models/Query';

import { authenticate } from './AuthMiddleware';
import Endpoint from './Endpoint';

export function configurePlayers(config: Config, db: DbWrapper): express.Router {
    const endpoint = new PlayersPage(config, db);
    const router = express.Router();
    const auth = authenticate(config.api.authBypass);

    router.get('/', endpoint.getPlayers);
    router.post('/migrate', auth, endpoint.migratePlayer);

    return router;
}

class PlayersPage extends Endpoint {
    migratePlayer = this.configureEndpoint(async (req: express.Request, res: Response) => {
        const fromUser = Query.username(req.query, 'fromName', true) as string;
        const toUser = Query.username(req.query, 'toName', true) as string;

        await this.db.rounds.renameUser(fromUser, toUser);

        const roundList = [...(await this.db.rounds.getRoundsForWinner(toUser))].sort((a, b) => a - b);

        const newPlayer: LeaderboardEntry = {
            username: toUser,
            numWins: roundList.length,
            roundList,
        };

        res.json({ newPlayer });
    });

    getPlayers = this.configureEndpoint(async (req: express.Request, res: Response) => {
        const roundsController = new RoundsController(this.db, res.locals.logger);
        const result = await roundsController.getPlayerMetrics({
            filterRounds: Query.extractStringOrThrow(req.query, 'filterRounds'),
            players: Query.array(req.query, 'names', true)!,
        });
        res.json(result);
    });
}
