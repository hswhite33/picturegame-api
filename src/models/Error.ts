export class ApiError {
    public message: string;
    constructor(public errorCode: string, public statusCode: number, public data?: object) {
        this.message = ErrorDesc[errorCode];
        Object.setPrototypeOf(this, ApiError.prototype);
    }
}

export class BadRequestError extends ApiError {
    private static readonly statusCode = 400;

    public static readonly RoundAlreadyExists = 'ROUND_ALREADY_EXISTS';
    public static readonly EmptyBodyError = 'EMPTY_BODY';
    public static readonly ValueRequired = 'VALUE_REQUIRED';
    public static readonly ValueMustBeObject = 'VALUE_MUST_BE_OBJECT';
    public static readonly ValueMustNotBeObject = 'VALUE_MUST_NOT_BE_OBJECT';
    public static readonly ValueMustBeArray = 'VALUE_MUST_BE_ARRAY';
    public static readonly InvalidPositiveInt = 'VALUE_MUST_BE_POSITIVE_INT';
    public static readonly InvalidUsername = 'INVALID_USERNAME';
    public static readonly UserAlreadyExists = 'USER_ALREADY_EXISTS';
    public static readonly InvalidDateTime = 'INVALID_DATE_TIME';
    public static readonly QueryParseError = 'QUERY_PARSE_ERROR';
    public static readonly InvalidSingleRequestType = 'INVALID_SINGLE_REQUEST_ENDPOINT';
    public static readonly ValidationFailed = 'VALIDATION_ERROR';

    constructor(errorCode: string, data?: object) {
        super(errorCode, BadRequestError.statusCode, data);
    }
}

export class UnauthorizedError extends ApiError {
    private static readonly statusCode = 401;

    public static readonly InvalidLogin = 'INVALID_LOGIN';
    public static readonly AuthRequired = 'AUTH_REQUIRED';

    constructor(errorCode: string, data?: object) {
        super(errorCode, UnauthorizedError.statusCode, data);
    }
}

export class NotFoundError extends ApiError {
    private static readonly statusCode = 404;

    public static readonly RoundNotFound = 'ROUND_NOT_FOUND';
    public static readonly InvalidRank = 'INVALID_RANK';
    public static readonly PlayerNotFound = 'PLAYER_NOT_FOUND';
    public static readonly UnknownRoundId = 'UNKNOWN_ROUND_ID';

    constructor(errorCode: string, data?: object) {
        super(errorCode, NotFoundError.statusCode, data);
    }
}

const ErrorDesc: Record<string, string> = {
    [BadRequestError.RoundAlreadyExists]: 'That round already exists. Consider using PATCH to update it.',
    [BadRequestError.EmptyBodyError]: 'You must provide a non-empty JSON object with this request.',
    [BadRequestError.ValueRequired]: 'The specified field is required.',
    [BadRequestError.ValueMustBeObject]: 'The specified field must be an object.',
    [BadRequestError.ValueMustBeArray]: 'The specified field must be an array.',
    [BadRequestError.ValueMustNotBeObject]: 'The specified field must not be an object.',
    [BadRequestError.InvalidPositiveInt]: 'The specified field must be a positive integer.',
    [BadRequestError.InvalidUsername]: 'The provided value is not a valid Reddit username.',
    [BadRequestError.UserAlreadyExists]: 'A user with that username already exists.',
    [BadRequestError.InvalidDateTime]: 'DateTimes must be provided in ISO-8601 format.',
    [BadRequestError.QueryParseError]: 'The provided query is invalid.',
    [BadRequestError.InvalidSingleRequestType]: 'The specified request endpoint is invalid.',
    [BadRequestError.ValidationFailed]: 'Validation failed.',

    [UnauthorizedError.AuthRequired]: 'This method requires an authenticated user.',
    [UnauthorizedError.InvalidLogin]: 'Invalid username or password.',

    [NotFoundError.RoundNotFound]: 'The round you requested was not found.',
    [NotFoundError.InvalidRank]: 'There is no player at the specified rank.',
    [NotFoundError.PlayerNotFound]: 'The specified player was not found.',
    [NotFoundError.UnknownRoundId]: 'The id for the specified round is unknown.',
};

export namespace ValidationErrorCode {
    export function ValueMustBePositiveInteger(fieldName: string) {
        return `${fieldName} must be a positive integer.`;
    }

    export function ValueMustBeNonNegativeInteger(fieldName: string) {
        return `${fieldName} must be a non-negative integer.`;
    }

    export function ValueMustBeValidDateTime(fieldName: string) {
        return `${fieldName} must be a valid datetime in ISO-8601 format.`;
    }

    export function ValueMustBeString(fieldName: string) {
        return `${fieldName} must be a string.`;
    }

    export function ValueMustBeBoolean(fieldName: string) {
        return `${fieldName} must be a boolean.`;
    }

    export function ValueMustBeArray(fieldName: string) {
        return `${fieldName} must be an array.`;
    }

    export const UsernameMustBeValid = 'Usernames must consist of up to twenty characters matching [A-Za-Z0-9-_].';
    export const TimestampMustBeValid = 'Timestamps must be positive integers, or string representations of positive integers.';
    export const RoundCannotBeWonByHost = 'The winnerName for a round cannot be the same as the hostName.';
    export const RoundMustEndAfterItBegins = 'The postTime for a round must be before the winTime.';
    export const RoundNumberRequired = 'All rounds must have a roundNumber.';
    export const RoundCannotBeCorrectedBeforeStart = 'A round cannot be +corrected before it starts.';
    export const RoundCannotBeCorrectedBeforeAnswer = 'A round cannot be +corrected before the answer is posted.';
    export const RoundCannotBeAbandonedBeforeItStarts = 'The abandonedTime for a round must be after the postTime.';
    export const RoundCannotBeAbandonedAfterItEnds = 'The winTime for a round cannot be before the abandonedTime.';

    export function BulkRequestHasInvalidNumRequests(maxRequests: number) {
        return `Bulk request must specify between 1 and ${maxRequests} requests.`;
    }
    export const SingleRequestMustBeObject = 'All single requests must be objects.';
}
