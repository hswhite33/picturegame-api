import { ExpressionTreeBuilder, FieldSpec, Tokenizer } from 'picturegame-api-wrapper';

import { getSelectedFields } from './FieldSelector';
import { getSortInfo } from './FieldSorter';
import { buildQuery, GroupQueryContext, QueryContext } from './QueryBuilder';

import { runWithBadRequest } from '../utils/QueryUtils';

export * from './QueryBuilder';

export function Filter<T>(
    input: string,
    context: QueryContext<T>,
    spec: FieldSpec<T>,
    filterType: 'where' | 'having' = 'where') {

    runWithBadRequest(() => {
        const tokens = new Tokenizer(input).tokenize();
        const expressionTree = new ExpressionTreeBuilder(tokens).read();
        buildQuery(expressionTree, context, spec, filterType);
    });
}

export function Select<T>(input: string[], context: QueryContext<T>, spec: FieldSpec<T>) {
    runWithBadRequest(() => getSelectedFields(input, context, spec));
}

export function Sort<T>(input: string[], context: QueryContext<T>, spec: FieldSpec<T>) {
    runWithBadRequest(() => {
        for (const line of input) {
            const sortInfo = getSortInfo(line, spec);
            context.orderBy(sortInfo.column as keyof T, sortInfo.direction);
        }
    });
}

export function GroupBy<T, S>(input: string, context: GroupQueryContext<T, S>) {
    runWithBadRequest(() => context.groupBy(input));
}
