import * as Knex from 'knex';
import * as moment from 'moment';

import {
    getTokenTypeOrThrow, isIdentifier, ArrayExpression, BaseExpression, CompoundExpression, CompOperatorType,
    ExpressionTree, FieldSpec, LogicalOperatorType, QueryParseError, RValueType, TokenType, ValidOperators,
} from 'picturegame-api-wrapper';

import { runWithBadRequest } from '../utils/QueryUtils';

export interface QueryResult<T> {
    count: number;
    results: T[];
}

export abstract class QueryContext<T> {
    constructor(public readonly db: Knex, public readonly query: Knex.QueryBuilder) {
    }

    protected prepare(): void { } // no-op, can be overridden by subclasses
    protected abstract convert(dbResult: any): T;

    abstract getColumnName(field: keyof T): string | Knex.Raw;
    abstract selectColumn(field: keyof T): void;

    orderBy(field: keyof T, direction: string): void {
        const column = this.getColumnName(field);
        // TODO: typings missing these too...

        // First sort - force nulls to the end
        this.query.orderBy(this.db.raw(`${column} is null`) as any);

        // Second sort - tie-break the non-nulls
        this.query.orderBy(column as any, direction);
    }

    async execute(limit?: number, offset?: number): Promise<QueryResult<T>> {
        this.prepare();

        const count = await this.count();

        limit && this.query.limit(limit);
        offset && this.query.offset(offset);
        const dbResults = await this.query;

        return {
            count,
            results: dbResults.map((r: any) => this.convert(r)),
        };
    }

    protected async count(): Promise<number> {
        const inner = this.query.clone().as('inner');
        const wrapped = this.db(inner).count();
        const result = await wrapped;
        return result[0]['count(*)'];
    }

    toString(): string {
        this.prepare();
        return this.query.toString();
    }
}

export interface GroupByRaw<T> {
    field: keyof T;
    args: string[];
}

export interface GroupBy<T> {
    field: keyof T;
    sql: string;
    args: string[];
}

export enum GroupByFieldType {
    ExactMatch,
    AbsoluteTime,
    RelativeTime,
    Duration,
}

const GroupByTypeNumArgs: Record<GroupByFieldType, number> = {
    [GroupByFieldType.ExactMatch]: 0,
    [GroupByFieldType.AbsoluteTime]: 2,
    [GroupByFieldType.RelativeTime]: 2,
    [GroupByFieldType.Duration]: 2,
};

function isTimeField(fieldType: GroupByFieldType) {
    return fieldType === GroupByFieldType.AbsoluteTime
        || fieldType === GroupByFieldType.RelativeTime
        || fieldType === GroupByFieldType.Duration;
}

export type GroupBySpec<T> = Partial<Record<keyof T, GroupByFieldType>>;

export enum GroupByTimeUnit {
    Year = 'year',
    Month = 'month',
    Week = 'week',
    Day = 'day',
    Hour = 'hour',
    Minute = 'minute',
    Second = 'second',
}

const GroupByArgRegex = /^([a-zA-Z]+)(?:\((.*)\))?$/;

export abstract class GroupQueryContext<TGroup, TEnt> extends QueryContext<TGroup> {
    protected groupByCol: GroupBy<TEnt>;

    groupBy(param: string) {
        const match = param.match(GroupByArgRegex);
        if (!match) {
            throw new QueryParseError('Invalid value for param groupBy');
        }

        const field = match[1] as keyof TEnt;
        const args = match[2] ? match[2].split(',').map(v => v.trim()) : [];

        this.groupByCol = {
            field: match[1] as keyof TEnt,
            sql: this.createGroupBySql(field, args),
            args,
        };
    }

    // Preconditions: field is verified as being the correct type; binSize is a valid number
    protected abstract getExactMatchGroupBySql(field: keyof TEnt): string;
    protected abstract getAbsoluteTimeGroupBySql(field: keyof TEnt, binSize: number, unit: GroupByTimeUnit): string;
    protected abstract getRelativeTimeGroupBySql(field: keyof TEnt, binSize: number, unit: GroupByTimeUnit): string;
    protected abstract getDurationGroupBySql(field: keyof TEnt, binSize: number, unit: GroupByTimeUnit): string;

    protected abstract getGroupBySpec(): GroupBySpec<TEnt>;

    private createGroupBySql(field: keyof TEnt, args: string[]): string {
        const spec = this.getGroupBySpec();
        const fieldType = spec[field];

        if (fieldType === undefined) {
            throw new QueryParseError(`Invalid field for groupBy: ${field}`);
        }

        const numArgs = GroupByTypeNumArgs[fieldType!];
        if (args.length !== numArgs) {
            throw new QueryParseError(`Invalid arguments for groupBy=${field}: expected ${numArgs}; got ${args.length}`);
        }

        if (fieldType === GroupByFieldType.ExactMatch) {
            return this.getExactMatchGroupBySql(field);
        }

        if (isTimeField(fieldType!)) {
            if (!args[0].match(/^[0-9]+$/)) {
                throw new QueryParseError(`Invalid binSize for groupBy: ${args[0]}`);
            }

            const binSize = parseInt(args[0], 10);
            if (isNaN(binSize) || binSize < 1) {
                throw new QueryParseError(`Invalid binSize for groupBy: ${args[0]}`);
            }

            const unit = args[1] as GroupByTimeUnit;

            switch (fieldType) {
                case GroupByFieldType.AbsoluteTime: return this.getAbsoluteTimeGroupBySql(field, binSize, unit);
                case GroupByFieldType.RelativeTime: return this.getRelativeTimeGroupBySql(field, binSize, unit);
                case GroupByFieldType.Duration: return this.getDurationGroupBySql(field, binSize, unit);
            }
        }

        throw new QueryParseError('Unable to parse value for groupBy');
    }
}

export function buildQuery<T>(
    tree: ExpressionTree,
    context: QueryContext<T>,
    spec: FieldSpec<T>,
    filterType: 'where' | 'having') {

    validateTree(tree, spec);
    context.query[filterType](builder => runWithBadRequest(() => compile(tree, builder, context)));
}

function validateTree<T>(tree: ExpressionTree, spec: FieldSpec<T>) {
    if (tree instanceof CompoundExpression) {
        validateTree(tree.lValue, spec);
        validateTree(tree.rValue, spec);
        return;
    }
    if (tree instanceof BaseExpression) {
        validateExpr(tree, spec);
    }
}

function validateExpr<T>(expr: BaseExpression, spec: FieldSpec<T>) {
    const { lValue, operator, rValue } = expr;

    const lValueType = getTokenTypeOrThrow(lValue, spec);
    const opType = operator.input as CompOperatorType;

    const validOperators = ValidOperators[lValueType];
    if (!validOperators.has(opType)) {
        throw new QueryParseError(
            `Invalid operator ${operator.input} for type ${lValueType} at index ${operator.index}`);
    }

    const operation = Operations[opType];
    if (!operation.validate(lValueType, rValue, spec as FieldSpec<any>)) {
        throw new QueryParseError(
            `Invalid value types ${lValueType} and ${rValue.type} for operator ${opType} at index ${lValue.index}`);
    }
}

function compile<T>(tree: ExpressionTree, builder: Knex.QueryBuilder, context: QueryContext<T>) {
    if (tree instanceof CompoundExpression) {
        builder.where(b => compile(tree.lValue, b, context));
        const rBuilder: Knex.QueryBuilder = tree.operator.input === LogicalOperatorType.And ? builder.and : builder.or;
        rBuilder.where(b => compile(tree.rValue, b, context));

    } else if (tree instanceof BaseExpression) {
        const lValue = context.getColumnName(tree.lValue.input as keyof T);
        const rValue = isIdentifier(tree.rValue)
            ? context.db.raw(context.getColumnName(tree.rValue.input as keyof T))
            : extractValue(tree.rValue);
        const opType = tree.operator.input as CompOperatorType;
        Operations[opType].where(builder, lValue, rValue);
    }
}

function extractValue(token: RValueType): any {
    if (token instanceof ArrayExpression) {
        return token.items.map(extractValue);
    }
    switch (token.type) {
        case TokenType.NumericValue:
            return parseInt(token.input, 10);
        case TokenType.StringValue:
            return token.input;
        case TokenType.DateTimeValue:
            const date = moment.utc(token.input, moment.ISO_8601, true);
            if (!date.isValid()) {
                throw new QueryParseError(`Invalid Date ${token.input} at index ${token.index}`);
            }
            return date.unix();
        case TokenType.BooleanValue:
            return token.input === 'true';
        case TokenType.TimeValue:
            return moment.duration(token.input).asSeconds();
        default:
            throw new QueryParseError(`Unexpected token type ${token.type} at index ${token.index}`);
    }
}

interface CompOperator {
    validate(lValueType: TokenType, rValue: RValueType, spec: FieldSpec<any>): boolean;
    where(builder: Knex.QueryBuilder, lValue: any, rValue: any): void;
}

function resolveValueType<T>(value: RValueType, spec: FieldSpec<T>) {
    if (value.type === TokenType.Identifier) {
        return getTokenTypeOrThrow(value, spec);
    }
    return value.type;
}

export const Operations: Record<CompOperatorType, CompOperator> = {
    [CompOperatorType.Equal]: {
        validate: (l, r, spec) => resolveValueType(r, spec) === l,
        where: (b, l, r) => b.where(l, r),
    },
    [CompOperatorType.NotEqual]: {
        validate: (l, r, spec) => resolveValueType(r, spec) === l,
        where: (b, l, r) => b.where(l, '!=', r),
    },
    [CompOperatorType.Greater]: {
        validate: (l, r, spec) => resolveValueType(r, spec) === l,
        where: (b, l, r) => b.where(l, '>', r),
    },
    [CompOperatorType.GreaterOrEqual]: {
        validate: (l, r, spec) => resolveValueType(r, spec) === l,
        where: (b, l, r) => b.where(l, '>=', r),
    },
    [CompOperatorType.Less]: {
        validate: (l, r, spec) => resolveValueType(r, spec) === l,
        where: (b, l, r) => b.where(l, '<', r),
    },
    [CompOperatorType.LessOrEqual]: {
        validate: (l, r, spec) => resolveValueType(r, spec) === l,
        where: (b, l, r) => b.where(l, '<=', r),
    },
    [CompOperatorType.Contains]: {
        validate: (_l, r) => r.type === TokenType.StringValue,
        where: (b, l, r) => b.where(l, 'like', `%${r}%`),
    },
    [CompOperatorType.NotContains]: {
        validate: (_l, r) => r.type === TokenType.StringValue,
        where: (b, l, r) => b.where(l, 'not like', `%${r}%`),
    },
    [CompOperatorType.In]: {
        validate: (l, r) => r instanceof ArrayExpression && l === r.arrayType,
        where: (b, l, r) => b.whereIn(l, r),
    },
    [CompOperatorType.NotIn]: {
        validate: (l, r) => r instanceof ArrayExpression && l === r.arrayType,
        where: (b, l, r) => b.whereNotIn(l, r),
    },
};
