import { Tokenizer } from 'picturegame-api-wrapper';

const SuccessfulTestInputs = [
    '',
    'identifiers _can h4v3 DIFFERENT c_h_a_r_s_',
    '"strings can use double quotes"',
    "'strings can use single quotes'",
    '2017-01-01',
    '2017-01-01T00:00:00Z',
    '2017-01-01T00:00:00+00:00',
    '0 00339 1000',
    'eq ne gt gte lt lte',
    'and or',
    '()',
    '(roundNumber gte 45000 and hostName eq "provium") or winTime lt 2018-02-01T02:34:43-06',
    'asc desc',
    ',, ,',
    '[ ] [] ][',
];

const ErrorTestInputs = [
    '"mismatched double quote',
    "'mismatched single quote",
    'unrecognized :',
    'bad date 2017-01',
];

describe('Tokenizer', () => {
    describe('successful state snapshot tests', () => {
        for (const input of SuccessfulTestInputs) {
            test(input, () => {
                expect(new Tokenizer(input).tokenize()).toMatchSnapshot();
            });
        }
    });

    describe('error state tests', () => {
        for (const input of ErrorTestInputs) {
            test(input, () => {
                function test() {
                    new Tokenizer(input).tokenize();
                }
                expect(test).toThrowErrorMatchingSnapshot();
            });
        }
    });
});
