import * as Knex from 'knex';
import { ExpressionTreeBuilder, Tokenizer } from 'picturegame-api-wrapper';

import { RoundFieldSpec, RoundQueryContext } from '../../db/RoundQueryContext';

import { buildQuery } from '../QueryBuilder';

const TestInputs = [
    'roundNumber eq "Provium"',
    'winnerName eq 2',
    'id eq 2017-01-01',
    'winTime eq winnerName',
    'winnerName gte "Provium"',
    'a eq 1',
    'hostName eq b',
    'postTime eq 2 or id eq "a"',
    'postTime eq 2018-01-01 or id eq 2',
    'title cont winnerName',
    'winDayOfWeek in ["wednesday"]',
    'title notin [winnerName]',
    'postTimeOfDay in [12:00:00]',
];

describe('Round query validator', () => {
    let knex: Knex;
    beforeAll(() => {
        knex = Knex({ client: 'sqlite3', useNullAsDefault: true });
    });

    for (const input of TestInputs) {
        test(input, () => {
            function test() {
                const context = new RoundQueryContext(knex);
                const tokens = new Tokenizer(input).tokenize();
                const tree = new ExpressionTreeBuilder(tokens).read();
                buildQuery(tree, context, RoundFieldSpec, 'where');
            }

            expect(test).toThrowErrorMatchingSnapshot();
        });
    }
});
