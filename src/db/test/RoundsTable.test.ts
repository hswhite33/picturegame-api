import { Round } from '../../models/model';

import * as utils from '../../tests/testutils';

import { DbWrapper } from '../DbWrapper';

describe('Rounds db wrapper unit tests', () => {
    let db: DbWrapper;

    beforeAll(async () => {
        db = await utils.getTestDb({},
            'INSERT INTO rounds(round_number, winner_name) VALUES (1, "Provium");',
        );
    });

    describe('getRound', () => {

        it('should get existing rounds', async () => {
            const round = await db.rounds.getSpecificRound(1);
            expect(round?.roundNumber).toBe(1);
        });

        it('should fail to get non-existant rounds', async () => {
            const round = await db.rounds.getSpecificRound(2);
            expect(round).toBeNull();
        });
    });

    describe('insert and delete rounds', () => {

        it('should add and remove rounds', async () => {
            await db.rounds.createRound({ roundNumber: 2 });
            const insertedRound = await db.rounds.getSpecificRound(2) as Round;
            expect(insertedRound.roundNumber).toBe(2);

            await db.rounds.deleteRound(2);
            const round = await db.rounds.getSpecificRound(2);
            expect(round).toBeNull();
        });

        it('should update existing rounds', async () => {
            await db.rounds.updateRound({ roundNumber: 1, hostName: 'abc' });
            const updatedRound = await db.rounds.getSpecificRound(1) as Round;
            expect(updatedRound.hostName).toBe('abc');
        });
    });
});
