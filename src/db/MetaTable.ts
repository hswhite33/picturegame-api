import * as Knex from 'knex';
import * as winston from 'winston';

import { MetaFields, TableName } from './model';

export class MetaTable {
    constructor(private db: Knex, private logger?: winston.Logger) {
    }

    public async createTable() {
        const logger = this.logger;
        logger?.info('Creating meta table + indexes');

        const exists = await this.db.schema.hasTable(TableName.Meta);
        if (exists) {
            logger?.info('Meta table already exists');
            return;
        }

        await this.db.schema.createTable(TableName.Meta, table => {
            table.text(MetaFields.TableName).notNullable().primary();
            table.integer(MetaFields.Version).notNullable();
        });

        logger?.info('Meta table created successfully');
    }
}
