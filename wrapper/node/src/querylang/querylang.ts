export enum TokenType {
    Invalid = 'INVALID',
    Identifier = 'IDENTIFIER',
    BooleanValue = 'VAL_BOOL',
    StringValue = 'VAL_STR',
    NumericValue = 'VAL_NUM',
    DateTimeValue = 'VAL_DATETIME',
    TimeValue = 'VAL_TIME',
    CompOperator = 'OP_COMPARISON',
    LogicalOperator = 'OP_LOGICAL',
    DirectionOperator = 'OP_DIRECTION',
    OpenBracket = 'BRACK_OPEN',
    CloseBracket = 'BRACK_CLOSE',
    OpenSqBrace = 'SQ_OPEN',
    CloseSqBrace = 'SQ_CLOSE',
    Comma = 'COMMA',
    Array = 'ARRAY',
}

export type ValueType =
    TokenType.BooleanValue |
    TokenType.StringValue |
    TokenType.NumericValue |
    TokenType.DateTimeValue |
    TokenType.TimeValue |
    TokenType.Invalid;

export enum CompOperatorType {
    Equal = 'eq',
    NotEqual = 'ne',
    Greater = 'gt',
    GreaterOrEqual = 'gte',
    Less = 'lt',
    LessOrEqual = 'lte',
    Contains = 'cont',
    NotContains = 'ncont',
    In = 'in',
    NotIn = 'notin',
}
export const CompOperators: ReadonlySet<string> = new Set<string>([
    CompOperatorType.Equal,
    CompOperatorType.NotEqual,
    CompOperatorType.Greater,
    CompOperatorType.GreaterOrEqual,
    CompOperatorType.Less,
    CompOperatorType.LessOrEqual,
    CompOperatorType.Contains,
    CompOperatorType.NotContains,
    CompOperatorType.In,
    CompOperatorType.NotIn,
]);

const OneToOneOperators: ReadonlySet<CompOperatorType> = new Set([
    CompOperatorType.Equal,
    CompOperatorType.NotEqual,
    CompOperatorType.Greater,
    CompOperatorType.GreaterOrEqual,
    CompOperatorType.Less,
    CompOperatorType.LessOrEqual,
]);

export const ValidOperators: Record<ValueType, ReadonlySet<CompOperatorType>> = {
    [TokenType.BooleanValue]: new Set([
        CompOperatorType.Equal,
    ]),
    [TokenType.StringValue]: new Set([
        CompOperatorType.Equal,
        CompOperatorType.NotEqual,
        CompOperatorType.Contains,
        CompOperatorType.NotContains,
        CompOperatorType.In,
        CompOperatorType.NotIn,
    ]),
    [TokenType.NumericValue]: new Set([
        ...OneToOneOperators,
        CompOperatorType.In,
        CompOperatorType.NotIn,
    ]),
    [TokenType.DateTimeValue]: OneToOneOperators,
    [TokenType.TimeValue]: OneToOneOperators,
    [TokenType.Invalid]: new Set(),
};

export enum LogicalOperatorType {
    And = 'and',
    Or = 'or',
}
export const LogicalOperators = new Set<string>([
    LogicalOperatorType.And,
    LogicalOperatorType.Or,
]);

export enum DirectionOperatorType {
    Asc = 'asc',
    Desc = 'desc',
}
export const DirectionOperators = new Set<string>([
    DirectionOperatorType.Asc,
    DirectionOperatorType.Desc,
]);

export enum BoolValueType {
    True = 'true',
    False = 'false',
}
export const BoolValues = new Set<string>([
    BoolValueType.True,
    BoolValueType.False,
]);

export interface IToken {
    type: TokenType;
    index: number;
    input: string;
}

export class QueryParseError extends Error { }

export type FieldSpec<T> = Record<keyof T, ValueType>;
export type ColumnLookup<T> = Record<keyof Required<T>, string>;

export function getTokenTypeOrThrow<T>(value: IToken, spec: FieldSpec<T>) {
    const tokenType = spec[value.input as keyof T];
    if (!tokenType) {
        throw new QueryParseError(`Unknown identifier ${value.input} at index ${value.index}`);
    }
    return tokenType;
}
